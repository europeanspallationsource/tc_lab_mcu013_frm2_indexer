﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.10">
  <POU Name="FB_ADS_MOTOR_COMM" Id="{6aa2cabe-a13b-4a88-a760-6966e1d32c85}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_ADS_MOTOR_COMM

VAR

    (* temporary values.... *)

 
    regRequest:      INT := 0;
	initialized: 	 BOOL := FALSE;

	ParamValue: INT;
	ParamControl: INT;

END_VAR
VAR_INPUT

END_VAR
VAR_OUTPUT

END_VAR
VAR_IN_OUT


    interf: ST_ParamOutput; (* PLC-SPEC interf *)
    persist:   ST_MotorPersist; (* persistent data *)
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[(* implements ParameterOutput interface for generic motor *)

initialized:= persist.initialized;
regRequest:= persist.regRequest;



IF (regRequest <> 0) OR NOT(initialized) THEN
  CASE regRequest OF
    (* Power-on reset *)
    0:      (* enable module - nothing to do for Beckhoff NC*)
             interf.Status := 16#1000;
			 initialized := TRUE;
  

    (* Soft reset / error reset *)
    100:   
        IF persist.reset_done 
		THEN
            interf.Status := 16#1000;
            regRequest := 0;
			persist.reset := FALSE;
			persist.reset_done := FALSE;
		END_IF
		

  END_CASE
END_IF

(* parameter interf handling *)
IF SHR(interf.Status, 12) = 6 AND SHR(interf.ParamControl, 13) = 2 THEN
    (* if BUSY, all param writes give ERR_RETRY_LATER *)
    interf.ParamControl := interf.ParamControl OR 16#E000;
ELSE
  IF regRequest = 0 THEN
    CASE interf.ParamControl OF
        0:      (* initial value *)
                interf.ParamControl := 16#8000; (* DONE, SUB=0, IDX=0 *)
                interf.ParamValue := 0.0;

        (* doRead *)
 
        16#203C: (* read Speed *)
                interf.ParamValue := persist.speed;
                interf.ParamControl := 16#803C; (* DONE, SUB=0, IDX=Speed *)

        16#203D: (* read Accel *)
                interf.ParamValue := persist.accel;
                interf.ParamControl := 16#803D; (* DONE, SUB=0, IDX=Accel *)

         16#2045 : (* read RefPos *)
                interf.ParamValue := 0;
                interf.ParamControl := 16#8045; (* DONE, SUB=0, IDX=RefPos *)


 

        (* doWrite *)
 
        16#403C : (* write Speed *)
                persist.Speed := interf.ParamValue;
                interf.ParamControl := 16#803C; (* DONE, SUB=0, IDX=Speed *)
  
        16#403D : (* write Accel *)
                persist.Accel := interf.ParamValue;
                interf.ParamControl := 16#803D; (* DONE, SUB=0, IDX=Accel *)




        (* CMD's *)
 
        16#4084 : (* CMD doRefPos *)
		 		persist.start_ref:= TRUE;
				persist.ref_done:= FALSE;
				interf.Status:= 16#6000; (* Set Interface to Busy *)
                interf.ParamControl := 16#6084; (* BUSY, SUB=0, IDX=doRefPos *)
                regRequest := 0;
        16#6084 : (* doing CMD doRefPos *)
				IF persist.ref_done THEN
					persist.start_ref:= FALSE;
					persist.ref_done:= FALSE;
					interf.ParamControl := 16#8084; (* DONE, SUB=0, IDX=doRefPos *)
					IF persist.error_code = 0 
					THEN 
						interf.Status:= 16#1000; // IDLE
				    ELSE 
					 	interf.Status:= 16#8000; //Error
					END_IF
				END_IF
  
        16#408E : (* CMD doContMove *)
 				persist.start_cont_mov:= TRUE; (* not yet implemented!!! *)
                 interf.ParamControl := 16#808E; (* DONE, SUB=0, IDX=doContMove *)
				 
        ELSE
        (* ERR_NO_IDX *)
        IF NOT interf.ParamControl.15 THEN
            interf.ParamControl := (interf.ParamControl AND 16#1FFF) OR 16#A000;
        END_IF
    END_CASE
  END_IF
END_IF


IF regRequest = 0 THEN
    CASE SHR(interf.Status, 12) OF
        0 : (* Reset (error-ACK) *)
            regRequest := 100; (* once finished this sets Status to Idle *)
			persist.reset := TRUE;
        1,3: (* idle, warn *)
             (* XXX: check enable? limit switches? *)

            interf.Status := 16#1000; (* IDLE *)
 
        5:  (* START *)
		    persist.target_pos := interf.Target;
 			persist.start_abs_mov := TRUE;
            interf.Status := 16#6000; (* BUSY *)
        6:  (* BUSY *)
            (* XXX: decode error bits! *)
            
			IF persist.start_abs_mov AND persist.abs_mov_done THEN
				persist.abs_mov_done := FALSE;
				persist.start_abs_mov := FALSE;
				IF persist.error_code = 0 
				THEN 
					interf.Status:= 16#1000; // IDLE
			    ELSE 
				 	interf.Status:= 16#8000; //Error
				END_IF
			END_IF
 
  			IF persist.stop AND persist.stop_done THEN
				persist.stop_done := FALSE;
				persist.stop := FALSE;
				interf.Status := 16#1000; (* IDLE *)
			END_IF
           

        7:  (* STOP *)
            IF NOT persist.stop_done THEN           
				persist.stop := TRUE;
            	interf.Status := 16#7000; (* STOP*)
			ELSE
				interf.Status := 16#1000; (* IDLE *)
				persist.stop_done := FALSE;
				persist.stop := FALSE;
				persist.abs_mov_done := FALSE;
				persist.start_abs_mov := FALSE;								
			END_IF
    ELSE
        (* ERROR *)
            // interf.Status := 16#1000; (* IDLE *)
    END_CASE
END_IF






(* transfer warning/error bits to AUX-bits *)
//Attention: Bit semantics must be known

interf.Value := persist.actual_pos;
IF persist.limit_pos THEN interf.Status:= interf.Status OR 16#0800; END_IF
IF persist.limit_neg THEN interf.Status:= interf.Status OR 16#0400; END_IF
IF persist.not_es_cw THEN interf.Status:= interf.Status OR 1; END_IF
IF persist.not_es_ccw THEN interf.Status:= interf.Status OR SHL(1,1); END_IF
IF persist.es_ref THEN interf.Status:= interf.Status OR SHL(1,2); END_IF
IF persist.es_mitte THEN interf.Status:= interf.Status OR SHL(1,3); END_IF
IF persist.moving THEN interf.Status:= interf.Status OR SHL(1,4); END_IF


persist.initialized := initialized;
persist.regRequest:= regRequest;

]]></ST>
    </Implementation>
    <LineIds Name="FB_ADS_MOTOR_COMM">
      <LineId Id="10" Count="0" />
      <LineId Id="926" Count="0" />
      <LineId Id="925" Count="0" />
      <LineId Id="928" Count="0" />
      <LineId Id="927" Count="0" />
      <LineId Id="11" Count="0" />
      <LineId Id="42" Count="4" />
      <LineId Id="50" Count="0" />
      <LineId Id="882" Count="0" />
      <LineId Id="144" Count="0" />
      <LineId Id="163" Count="2" />
      <LineId Id="195" Count="0" />
      <LineId Id="992" Count="0" />
      <LineId Id="196" Count="1" />
      <LineId Id="995" Count="1" />
      <LineId Id="993" Count="1" />
      <LineId Id="198" Count="0" />
      <LineId Id="476" Count="14" />
      <LineId Id="517" Count="0" />
      <LineId Id="519" Count="7" />
      <LineId Id="909" Count="2" />
      <LineId Id="541" Count="1" />
      <LineId Id="554" Count="2" />
      <LineId Id="588" Count="0" />
      <LineId Id="591" Count="3" />
      <LineId Id="600" Count="3" />
      <LineId Id="679" Count="3" />
      <LineId Id="704" Count="0" />
      <LineId Id="918" Count="0" />
      <LineId Id="923" Count="0" />
      <LineId Id="945" Count="1" />
      <LineId Id="919" Count="0" />
      <LineId Id="935" Count="0" />
      <LineId Id="937" Count="0" />
      <LineId Id="939" Count="1" />
      <LineId Id="942" Count="0" />
      <LineId Id="989" Count="0" />
      <LineId Id="947" Count="0" />
      <LineId Id="987" Count="1" />
      <LineId Id="984" Count="0" />
      <LineId Id="986" Count="0" />
      <LineId Id="985" Count="0" />
      <LineId Id="941" Count="0" />
      <LineId Id="920" Count="0" />
      <LineId Id="716" Count="0" />
      <LineId Id="719" Count="1" />
      <LineId Id="924" Count="0" />
      <LineId Id="722" Count="13" />
      <LineId Id="991" Count="0" />
      <LineId Id="737" Count="3" />
      <LineId Id="773" Count="1" />
      <LineId Id="884" Count="0" />
      <LineId Id="786" Count="3" />
      <LineId Id="798" Count="0" />
      <LineId Id="949" Count="1" />
      <LineId Id="952" Count="0" />
      <LineId Id="1000" Count="5" />
      <LineId Id="951" Count="0" />
      <LineId Id="799" Count="0" />
      <LineId Id="954" Count="3" />
      <LineId Id="800" Count="4" />
      <LineId Id="958" Count="0" />
      <LineId Id="881" Count="0" />
      <LineId Id="959" Count="0" />
      <LineId Id="962" Count="0" />
      <LineId Id="960" Count="0" />
      <LineId Id="963" Count="0" />
      <LineId Id="1033" Count="1" />
      <LineId Id="961" Count="0" />
      <LineId Id="805" Count="1" />
      <LineId Id="811" Count="5" />
      <LineId Id="829" Count="3" />
      <LineId Id="835" Count="1" />
      <LineId Id="839" Count="0" />
      <LineId Id="964" Count="0" />
      <LineId Id="943" Count="0" />
      <LineId Id="972" Count="0" />
      <LineId Id="979" Count="3" />
      <LineId Id="975" Count="0" />
      <LineId Id="930" Count="1" />
      <LineId Id="929" Count="0" />
      <LineId Id="932" Count="0" />
      <LineId Id="9" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>